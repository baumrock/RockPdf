<?php namespace ProcessWire;
/**
 * Module info file that tells ProcessWire about this module. 
 */
$info = [
  'title' => 'mPDF Helper Module',
  'version' => '1.0.1',
  'summary' => 'A helper module for the mPDF library',
  'singular' => false,
  'autoload' => false,
  'icon' => 'file-pdf-o',
];